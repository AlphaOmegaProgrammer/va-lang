#![warn(clippy::all, clippy::nursery, clippy::pedantic, clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]

#![no_main]


use va_lang::types;
use va_lang::lex_and_parse;


#[no_mangle]
pub extern fn main(argc: i32, argv: *const *const core::ffi::c_char){
    #[allow(non_snake_case)]
    let mut AST: Vec<&types::ASTNode> = Vec::new();

    let mut files: Vec<types::File> = Vec::<types::File>::new();
    let mut i:usize = 1;

    while i < argc as usize{
        let arg: &core::ffi::CStr = unsafe{ core::ffi::CStr::from_ptr(*(argv.wrapping_add(i))) };
        let arg_slice: &str = arg.to_str().expect("Argument is not a valid UTF-8 string");

        if arg_slice.starts_with('-'){
            // Handle options here
        }else{
             match std::fs::read_to_string(&arg_slice){
                Ok(file_contents) => {
                    lex_and_parse::lex_and_parse(&AST, &arg, &file_contents);

                    files.push(types::File{
                        name: &arg,
                        contents: file_contents
                    });
                },

                Err(e) => println!("Error compiling {arg_slice}: {e}")
            };
        }

        i += 1;
    }

    println!("AST:\t{AST:#?}");
}
