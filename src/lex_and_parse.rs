use crate::types;

#[allow(non_snake_case)]
pub fn lex_and_parse(AST: &Vec<&types::ASTNode>, file_name: &core::ffi::CStr, input: &str){
    let mut i: usize = 0;
    let input_length: usize = input.len();

    let mut current_expression: Option<&types::ASTNode> = None;

    let mut line_number: u32 = 1;
    let mut line_offset: u32 = 1;
    let mut line_absolute_start: u64 = 0;

    while i < input_length{
        let start_i: usize = i;
        i += 1;

        let t: types::TerminalType = match &input[start_i..i]{
            " "|"\t" => continue,

            "\n" => {
                line_number += 1;
                line_absolute_start = (start_i + 1) as u64;
                line_offset = 1;
                continue;
            }


            ";" => types::TerminalType::Semicolon,
            "=" => types::TerminalType::AssignmentOperator,


            "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9" => {
                loop {
                    match &input[i..].chars().next() {
                        Some('0'..='9') => i += 1,
                        _ => break
                    };
                }

                types::TerminalType::IntLiteral
            },


            _ => {
                if &input[start_i..start_i+3] == "let"{
                    i += 2;
                    types::TerminalType::LetKeyword
                }else{
                    loop {
                        match &input[i..].chars().next() {
                            Some('a'..='z'| 'A'..='Z'| '0'..='9' | '_') => i += 1,
                            _ => break
                        };
                    }

                    types::TerminalType::Symbol
                }
            }
        };

        let tnode: types::TerminalNode = types::TerminalNode {
            ttype: t,

            file_name: file_name,
            text: &input[start_i..i],

            line_absolute_start: line_absolute_start,
            char_absolute_position: start_i as u64,

            line_number: line_number,
            char_position: line_offset
        };

        line_offset += (i - start_i + 1) as u32;
    }
}
