#[derive(Debug)]
pub struct File<'a>{
    pub name: &'a core::ffi::CStr,
    pub contents: String
}

#[derive(Debug)]
pub struct TerminalNode<'a,'b>{
    pub ttype: TerminalType,

    pub file_name: &'a core::ffi::CStr,

    pub text: &'b str,

    pub line_absolute_start: u64,
    pub char_absolute_position: u64,

    pub line_number: u32,
    pub char_position: u32
}

#[derive(Debug)]
pub struct ASTNode<'a, 'b>{
    pub gtype: Option<GrammarType>,

    pub terminal: Option<&'a TerminalNode<'a, 'b>>,

    pub children: Vec<&'a ASTNode<'a, 'b>>
}



#[derive(Debug)]
pub enum TerminalType{
    LetKeyword,
    AssignmentOperator,
    IntLiteral,
    Semicolon,
    Symbol
}

#[derive(Debug)]
pub enum GrammarType{
    GeneralExpression,
    AssignmentExpression
}
